FactoryBot.define do
  factory :task do
    title { "MyString" }
    description { "MyText" }
    board { nil }
  end
end
