FactoryBot.define do
  factory :board do
    title { "MyString" }
    description { "MyText" }
    project { nil }
  end
end
