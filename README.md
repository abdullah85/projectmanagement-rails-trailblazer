# Rails Project Mgmt


## API for Project Management

1) Create a backend rest api for Project Management using the ruby framework and mysql database
2) Models - Project, Stage, Tasks. Project consist of multiple Stages(Boards) and each Stage has multiple tasks allocated to it.
3) CRUD operations - for Project, Stage, Task

## Trailblazer Enhancement

Using Trailblazer, we have upgraded the Project controller, model to ensure operation is implemented separately.

## CRUD Implementation Details

CRUD involves creating project, stage, tasks with the following requirements

a) Project Name, Description to be provided while creating project
b) Stage Name, Description, Project ID to be provided while creating Stages
c) Tasks Name, Description, Stage ID to be provided to create a Task

All above constraints are while creating project, stage, task

For Read operation, we need to implement
- Read list/index of all projects
- List of Stages given a particular project id
- List of Tasks for a particular Stage id.

## Installation

## Usage

<!--
	Use examples liberally, and show the expected output if you can.
	It's helpful to have inline the smallest example of usage that you can demonstrate,
	     while providing links to more sophisticated examples if they are too long to reasonably include in the README.
-->


## Roadmap

## Contributing

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status


# From Rails Setup

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


