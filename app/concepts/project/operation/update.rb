module Project::Operation
    class Update < Trailblazer::Operation

        step Model(Project, :find_by)
        step :assign_model
        step :validate_model
        # step :notifyValidated
        step Contract::Build(constant: Project::Contract::Create)
        # step Contract::Validate()
        step Contract::Persist()
        step :notify!

        def assign_model(ctx, **)
            projParams = ctx[:params][:project]
            ctx[:model].title = projParams['title']
            ctx[:model].description = projParams['description']
        end

        def validate_model(ctx, **)
            # debugger
            model = ctx[:model]
            if( model[:title] == nil or model[:title] == '')
                ctx["result.notify"] = Rails.logger.info("Invalid title : #{model.title}.")
                return false
            end
            if( model[:description].length < 10 )
                ctx["result.notify"] = Rails.logger.info("Invalid description : #{model.title}.")
                return false
            end
            true
            # @model.saves
        end

        def notify!(ctx, model:, **)
            # debugger
            ctx["result.notify"] = Rails.logger.info("Updated Project with id : #{model.id}.")
        end
    end
end