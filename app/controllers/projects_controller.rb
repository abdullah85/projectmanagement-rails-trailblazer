class ProjectsController < ApplicationController
  def index
    @projects = Project.all
  end

  def show
    @project = Project.find(params[:id])
  end

  def new
    @project = Project.new
  end

  def create
    run Project::Operation::Create do |result|
      # debugger
      return redirect_to result[:model]
    end
    # debugger
    # @result = Projects::Create.(params:{})
    # endpoint Projects::Create
    render :new
  end

  def edit
    @project = Project.find(params[:id])
  end

  def update
    run Project::Operation::Update do |result|
      # debugger
      return redirect_to result[:model]
    end
    render :edit, status: :unprocessable_entity
  end

  def destroy
    @project = Project.find(params[:id])
    @project.destroy

    redirect_to root_path, status: :see_other
  end

  private
  def project_params
    params.require(:project).permit(:title, :description)
  end
end
